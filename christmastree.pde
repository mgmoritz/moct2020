ArrayList<Ball> balls = new ArrayList<Ball>();
float angle = 0;
void setup() {
  size(400, 400, P3D);
  addBalls();
  frameRate(40);
}

private color getColor(int index, Light light) {
  color[] colorsOn = {
    color(255, 0, 0),
    color(255, 255, 0),
    color(0, 255, 0)
  };

  color[] colorsOff = {
    color(155, 80, 80),
    color(155, 155, 80),
    color(80, 155, 80)
  };

  if (light == Light.ON) {
    return colorsOn[index % 3];
  } else {
    return colorsOff[index % 3];
  }
}

int getNumberOfBallsInARow(int rowNumber) {
  return rowNumber + 2 * (rowNumber - 1);
}

void addBalls() {
  int x = 0;
  int y = 20;
  int ballIndex = 0;

  for (int i = 1; i < 20; i++) {
    int ballsInThisRow = getNumberOfBallsInARow(i);
    float stepAngle = 2 * PI / ballsInThisRow;
    for (int j = 0; j < ballsInThisRow; j++) {
      PVector position = new PVector(x, y);
      float angle = stepAngle * j;
      color onColor = getColor(ballIndex, Light.ON);
      color offColor = getColor(ballIndex, Light.OFF);
      int size = 3;

      int framesOn = 10;
      int framesOff = 20;

      Ball ball = new Ball(position, size, onColor, offColor,
                           framesOn, framesOff, angle);
      if (ballIndex % 3 == 0) {
        ball.toggleLight();
        ball.setFrameCount(10);
      }

      if (ballIndex % 3 == 1)
        ball.toggleLight();

      balls.add(ball);

      ballIndex++;
    }
    x += 6;
    y += 17;
  }
}

void draw() {
  background(0);
  float stepRotation = 0.01;

  lights();
  for(Ball b : balls) {
    b.draw();
  }
}
