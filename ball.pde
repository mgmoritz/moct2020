import java.util.Dictionary;
import java.util.Hashtable;

public class Ball {
  private static final float STEP_ROTATION = 0.01;
	Dictionary<Light, Integer> _frameCounts = new Hashtable<Light, Integer>();

  public PVector _pos;
  public int _size;

  public color _onColor;
  public color _offColor;
  public Light _light;
  public float _angle;
  public int _frameCount;

  public Ball(PVector pos, int size, color onColor, color offColor, int framesOn, int framesOff, float angle) {
    _pos = pos;
    _size = size;
    _onColor = onColor;
    _offColor = offColor;
    _light = Light.ON;
		_frameCounts.put(Light.ON, framesOn);
		_frameCounts.put(Light.OFF, framesOff);
    _angle = angle;
  }

  public void setFrameCount(int frameCount) {
    _frameCount = frameCount;
  }

  public void toggleLight() {
    if (_light == Light.ON) {
      _light = Light.OFF;
    } else {
      _light = Light.ON;
    }
    _frameCount = 0;
  }

  public void rotate(float angle) {
    _angle += angle;
  }

  public void draw() {
    rotate(STEP_ROTATION);
    configureDraw();
    executeDraw();

  }

  private void configureDraw() {
    pushMatrix();
    translate(width/2, 0);
    rotateY(_angle);
    translate(_pos.x, _pos.y);
  }

  private void executeDraw() {
    controlLight();
    dim();
    glow();
    closeDraw();
  }

  private void closeDraw() {
    popMatrix();
  }

  private void controlLight() {
    _frameCount++;
		if (_frameCount >= _frameCounts.get(_light)) {
			toggleLight();
		}
  }

  private void dim() {
    noStroke();
    if (_light == Light.OFF) {
      fill(_offColor);
      sphere(_size);
    }
  }

  private void glow() {
    noStroke();
    if (_light == Light.ON) {
      fill(_onColor);
      sphere(_size);
      color glowColor = color(red(_onColor),
                              green(_onColor),
                              blue(_onColor),
                              80);
      fill(glowColor);
      float glowSize = 1.5;
      sphere(glowSize * _size);
    }
  }
}
