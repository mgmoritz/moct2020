.PHONY = all
all: ## Run processing sketch with processing-java
	cd .. && /home/moritz/workspace/processing/processing-java --sketch=/home/moritz/sketchbook/projects/christmastree --run

help:
	@echo ""
	@echo "Usage: make [target]"
	@echo ""
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'
	@echo ""
